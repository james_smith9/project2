# PROJECT NAME

## Project Description

An Appian Website that creates and manages employees within an organization

## Technologies Used

* Appian - version 20.4

## Features

* Create Employees
* Access Employee Information
* Update Employee Information

To-do list:
* Add Employees to different supervisor roles
